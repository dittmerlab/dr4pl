---
title: "Untitled"
author: "Justin L"
date: "2018-03-20"
output: html_document
---


##Test environments
* local OS X El Capitan, R 4.1.0
* CentOS 7, R 3.6.0
* Windows 10, R 4.1.0


## R CMD check results
There were no Errors, or Notes.
    
## Downstream dependencies
There are currently no downstream dependencies for this package.
